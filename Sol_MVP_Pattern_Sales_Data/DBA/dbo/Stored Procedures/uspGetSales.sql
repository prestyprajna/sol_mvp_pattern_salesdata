﻿CREATE PROC uspGetSales
(
	@Command Varchar(50)=NULL,
	@SalesOrderId int =NULL
)
AS
	BEGIN

	DECLARE @ErrorMessage Varchar(max)

		IF @Command='GetSalesHeaderData'
		BEGIN

			--BEGIN TRANSACTION

			--BEGIN TRY
				SELECT SOH.SalesOrderID,
				SOH.RevisionNumber,
				SOH.OrderDate
					FROM Sales.SalesOrderHeader AS SOH
						WHERE SOH.SalesOrderID=@SalesOrderId

				--COMMIT TRANSACTION


			--END TRY

			--BEGIN CATCH 
			--	SET @ErrorMessage=ERROR_MESSAGE()
			--	ROLLBACK TRANSACTION

			--	RAISERROR(@ErrorMessage,16,1)

			--END CATCH

		
		END
		ELSE IF @Command='GetSalesDetailData'
		BEGIN

			--BEGIN TRANSACTION

			--BEGIN TRY
				SELECT SOD.SalesOrderID,
				SOD.OrderQty,
				SOD.UnitPrice
					FROM Sales.SalesOrderDetail AS SOD
						WHERE SOD.SalesOrderID=@SalesOrderId


			--END TRY

			--BEGIN CATCH 
			--	SET @ErrorMessage=ERROR_MESSAGE()
			--	ROLLBACK TRANSACTION

			--	RAISERROR(@ErrorMessage,16,1)

			--END CATCH

		END

	END
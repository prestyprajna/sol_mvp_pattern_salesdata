﻿CREATE PROCEDURE uspGetSalesOrder
(
	@Command VARCHAR(MAX),
	@SalesOrderID NUMERIC(18,0),

	@Status INT OUT,
	@Message VARCHAR(MAX) OUT
)
AS
	BEGIN

	--DECLARATION
	DECLARE @ErrorMessage VARCHAR(MAX)

		IF @Command='SELECT_HEADER_DATA'
		BEGIN

			BEGIN TRANSACTION

			BEGIN TRY

				SELECT SOH.SalesOrderID,
				SOH.SalesOrderNumber,
				SOH.OrderDate,
				SOH.AccountNumber AS 'AccountNo',
				SC.AccountNumber AS 'CustomerAccountNo',
				ST.Name,
				ST.[CountryRegionCode],
				ST.[Group],
				SOH.SubTotal,
				SOH.TaxAmt,
				SOH.Freight
					FROM Sales.SalesOrderHeader AS SOH
						LEFT JOIN
							Sales.Customer SC				
								ON 
									SOH.CustomerID=SC.CustomerID
										LEFT JOIN
											Sales.SalesTerritory AS ST
												ON 
													SOH.TerritoryID=ST.TerritoryID														
														WHERE SOH.SalesOrderID=@SalesOrderID
				

				SET @Status=1
				SET @Message='SELECT HEADER DATA SUCCESSFULL'
			
					COMMIT TRANSACTION
			END TRY

			BEGIN CATCH

				SET @ErrorMessage=ERROR_MESSAGE()
				SET @Status=0
				SET @Message='SELECT HEADER DATA EXCEPTION'
			
				ROLLBACK TRANSACTION
				RAISERROR(@ErrorMessage,16,1)

			END CATCH

			
		END

		ELSE IF @Command='SELECT_DETAILS_DATA'
		BEGIN

		BEGIN

			BEGIN TRANSACTION

			BEGIN TRY				

				SELECT SOD.SalesOrderID,
				SOD.OrderQty,
				SOD.UnitPrice
				FROM Sales.SalesOrderDetail AS SOD
					WHERE SOD.SalesOrderID=@SalesOrderID
															

				SET @Status=1
				SET @Message='SELECT DETAILS DATA SUCCESSFULL'
			
					COMMIT TRANSACTION
			END TRY

			BEGIN CATCH

				SET @ErrorMessage=ERROR_MESSAGE()
				SET @Status=0
				SET @Message='SELECT DETAILS DATA EXCEPTION'
			
				ROLLBACK TRANSACTION
				RAISERROR(@ErrorMessage,16,1)

			END CATCH

			
		END


		END

	END
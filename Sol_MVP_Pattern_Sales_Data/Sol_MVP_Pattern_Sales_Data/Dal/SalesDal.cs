﻿using Sol_MVP_Pattern_Sales_Data.Dal.ORD;
using Sol_MVP_Pattern_Sales_Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_MVP_Pattern_Sales_Data.Dal
{
    public class SalesDal
    {
        #region  declaration
        private SalesDCDataContext _dc = null;

        #endregion

        #region  constructor
        public SalesDal()
        {
            _dc = new SalesDCDataContext();
        }

        #endregion

        #region  public methods

        public async Task<SalesOrderHeaderEntity> GetSalesHeaderData(SalesOrderHeaderEntity entityObj)
        {
            int? status = null;
            string message = null;

            return await Task.Run(() =>
            {
                var getdata =
                _dc?.uspGetSalesOrder(
                    "SELECT_HEADER_DATA",
                    entityObj?.SalesOrderID,
                    ref status,
                    ref message
                    )
                    ?.AsEnumerable()
                    ?.Select((leHeaderObj) => new SalesOrderHeaderEntity()
                    {
                        SalesOrderID = leHeaderObj?.SalesOrderID,
                        SalesOrderNumber = leHeaderObj?.SalesOrderNumber,
                        OrderDate = Convert.ToString(leHeaderObj?.OrderDate),
                        AccountNumber = leHeaderObj?.AccountNo,
                        AccountNumber1 = leHeaderObj?.CustomerAccountNo,
                        Name = leHeaderObj?.Name,
                        CountryRegionCode = leHeaderObj?.CountryRegionCode,
                        Group = leHeaderObj?.Group,
                        SubTotal = leHeaderObj?.SubTotal,
                        TaxAmt = leHeaderObj?.TaxAmt,
                        Freight = leHeaderObj?.Freight,
                        SalesOrderDetailsObj = this.GetSalesDetailsData(Convert.ToInt32(entityObj?.SalesOrderID)).Result as List<SalesOrderDetailsEntity>
                    })
                    ?.FirstOrDefault();

                return getdata;

            });
        }

        private async Task<IEnumerable<SalesOrderDetailsEntity>> GetSalesDetailsData(int? salesOrderId)
        {
            int? status = null;
            string message = null;            

            return await Task.Run(() =>
            {
                var getdata =
               _dc?.uspGetSalesOrder(
                    "SELECT_DETAILS_DATA",
                    salesOrderId,
                    ref status,
                    ref message)
                    ?.AsEnumerable()
                    ?.Where((leDetailsObj)=>leDetailsObj.SalesOrderID==salesOrderId)
                    ?.Select((leDetailsObj) => new SalesOrderDetailsEntity()
                    {
                        SalesOrderID = leDetailsObj?.SalesOrderID,
                        OrderQty = leDetailsObj?.OrderQty,
                        UnitPrice = leDetailsObj?.UnitPrice
                    })
                    ?.ToList();

                return getdata;
                   
            });
        }
     
        public async Task<int?> GetDetailsCount(int salesOrderId)   //apply pagination only to details data
        {
            return await Task.Run(async() =>
            {
                var getDetailsCount = (await this.GetSalesDetailsData(salesOrderId))
                ?.AsEnumerable()
                ?.Count((leSalesOrderObj) => leSalesOrderObj.SalesOrderID == salesOrderId);

                return getDetailsCount;
            });
        }

        public async Task<IEnumerable<SalesOrderDetailsEntity>> SalesDetailsDataPagination(int salesOrderId,int startRowIndex,int maximumRows)
        {
            return await Task.Run(async() =>
            {
                var getPaginationData =
                (await this.GetSalesDetailsData(salesOrderId))
                ?.AsEnumerable()
                ?.Skip(startRowIndex)
                ?.Take(maximumRows)
                ?.ToList();

                return getPaginationData;
            });

        }

        #endregion
    }
}
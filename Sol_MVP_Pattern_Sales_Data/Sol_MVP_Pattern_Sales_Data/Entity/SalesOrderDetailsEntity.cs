﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_MVP_Pattern_Sales_Data.Entity
{
    public class SalesOrderDetailsEntity
    {
        public int? SalesOrderID { get; set; }

        public short? OrderQty { get; set; }

        public decimal? UnitPrice { get; set; }
    }
}
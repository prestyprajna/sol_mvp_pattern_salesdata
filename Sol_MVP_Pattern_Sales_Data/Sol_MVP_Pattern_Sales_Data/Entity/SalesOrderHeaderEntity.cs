﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_MVP_Pattern_Sales_Data.Entity
{
    public class SalesOrderHeaderEntity
    {
        public int? SalesOrderID { get; set; }

        public string SalesOrderNumber { get; set; }

        public string OrderDate { get; set; }

        public string AccountNumber { get; set; }

        public string AccountNumber1 { get; set; }

        public string Name { get; set; }

        public string CountryRegionCode { get; set; }

        public string Group { get; set; }

        public decimal? SubTotal { get; set; }

        public decimal? TaxAmt { get; set; }

        public decimal? Freight { get; set; }

        public List<SalesOrderDetailsEntity> SalesOrderDetailsObj { get; set; }
    }
}
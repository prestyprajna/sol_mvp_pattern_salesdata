﻿<%@ Page Language="C#" AutoEventWireup="true" Async="true" CodeBehind="Default.aspx.cs" Inherits="Sol_MVP_Pattern_Sales_Data.View.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

      <%-- <style type="text/css">

        table
        {
            margin:auto;
            padding:10px;
            width:50%;
        }

    </style>--%>

</head>
<body>
    <form id="form1" runat="server">
    <div>

        <asp:ScriptManager ID="scriptManager" runat="server"></asp:ScriptManager>

        <asp:UpdatePanel ID="updatePanel" runat="server">
            <ContentTemplate>

                <table>

                    <tr>
                        <td>

                            <tr>
                                <td>
                                    <asp:TextBox ID="txtSearchSalesOrderNo" runat="server" TextMode="Search"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <asp:Label ID="lblSalesOrderNumber" runat="server" Text="SalesOrderNumber : "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSalesOrderNumber" runat="server"></asp:TextBox>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <asp:Label ID="lblOrderDate" runat="server" Text="OrderDate : "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtOrderDate" runat="server"></asp:TextBox>

                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <asp:Label ID="lblAccountNo" runat="server" Text="AccountNo : "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAccountNo" runat="server"></asp:TextBox>

                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <asp:Label ID="lblCustomerAccountNo" runat="server" Text="CustomerAccountNo : "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCustomerAccountNo" runat="server"></asp:TextBox>

                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <asp:Label ID="lblTerritoryName" runat="server" Text="TerritoryName : "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtTerritoryName" runat="server"></asp:TextBox>

                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <asp:Label ID="lblCountry" runat="server" Text="Country : "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCountry" runat="server"></asp:TextBox>

                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <asp:Label ID="lblCountryGroup" runat="server" Text="CountryGroup : "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCountryGroup" runat="server"></asp:TextBox>

                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <asp:Label ID="lblSubTotal" runat="server" Text="SubTotal : "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSubTotal" runat="server"></asp:TextBox>

                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <asp:Label ID="lblTaxAmt" runat="server" Text="TaxAmt : "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtTaxAmt" runat="server"></asp:TextBox>

                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <asp:Label ID="lblFreight" runat="server" Text="Freight : "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFreight" runat="server"></asp:TextBox>

                                </td>
                            </tr>

                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:GridView ID="gvSalesDetails" runat="server" ItemType="Sol_MVP_Pattern_Sales_Data.Entity.SalesOrderDetailsEntity" SelectMethod="gvSalesDetails_GetData" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="SalesOrderID" GridLines="Horizontal" PageSize="5">

                                <AlternatingRowStyle BackColor="#F7F7F7" />
                                <Columns>
                                    <asp:TemplateField HeaderText="OrderQty">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOrderQty" runat="server" Text='<%# Item.OrderQty %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="SalesOrderID" Visible="False"></asp:TemplateField>

                                    <asp:TemplateField HeaderText="UnitPrice">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUnitPrice" runat="server" Text='<%# Item.UnitPrice %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    No Records Found
                                </EmptyDataTemplate>
                                <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <SortedAscendingCellStyle BackColor="#F4F4FD" />
                                <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                                <SortedDescendingCellStyle BackColor="#D8D8F0" />
                                <SortedDescendingHeaderStyle BackColor="#3E3277" />

                            </asp:GridView>
                        </td>
                    </tr>




                </table>

            </ContentTemplate>
        </asp:UpdatePanel>
    
    
    </div>
    </form>
</body>
</html>

﻿using Sol_MVP_Pattern_Sales_Data.Dal;
using Sol_MVP_Pattern_Sales_Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_MVP_Pattern_Sales_Data.View
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected async void btnSearch_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(txtSearchSalesOrderNo.Text);

            SalesOrderHeaderEntity entityObj = new SalesOrderHeaderEntity()
            {
                SalesOrderID = id
            };

            SalesOrderHeaderEntity entityObj1 = await new SalesDal().GetSalesHeaderData(entityObj);

            await this.DataMapping(entityObj1);
        }

        public async Task DataMapping(SalesOrderHeaderEntity entityObj1)
        {
             await Task.Run(() =>
            {
                this.SearchSalesOrderNoBinding = Convert.ToInt32(entityObj1?.SalesOrderID);
                this.SalesOrderNumberBinding = entityObj1?.SalesOrderNumber;
                this.OrderDateBinding = entityObj1?.OrderDate;
                this.AccountNoBinding = entityObj1?.AccountNumber;

                this.CustomerAccountNoBinding = entityObj1?.AccountNumber1;
                this.TerritoryNameBinding = entityObj1?.Name;
                this.CountryBinding = entityObj1?.CountryRegionCode;
                this.CountryGroupBinding = entityObj1?.Group;


                this.SubTotalBinding = entityObj1?.SubTotal;
                this.TaxAmtBinding = entityObj1?.TaxAmt;
                this.FreightBinding = entityObj1?.Freight;
            });
        }

        // The return type can be changed to IEnumerable, however to support
        // paging and sorting, the following parameters must be added:
        //     int maximumRows
        //     int startRowIndex
        //     out int totalRowCount
        //     string sortByExpression
        public async Task<SelectResult> gvSalesDetails_GetData([Control("txtSearchSalesOrderNo")] string SalesID,int startRowIndex,int maximumRows)
        {            

            int? iD = Convert.ToInt32(SalesID);

            if (iD==0)
            {
                iD = null;
            }

            SalesOrderHeaderEntity entityObj = new SalesOrderHeaderEntity()
            {
                SalesOrderID = iD
            };

            //SearchSalesOrderNoBinding = Convert.ToInt32(iD);

            //int id = Convert.ToInt32(iD);



            if(iD != null)
            {
                SalesOrderHeaderEntity entityObj1 = await new SalesDal().GetSalesHeaderData(entityObj);

                var count = await new SalesDal().GetDetailsCount(Convert.ToInt32(iD));

                IEnumerable<SalesOrderDetailsEntity> getData = await new SalesDal().SalesDetailsDataPagination(Convert.ToInt32(iD),startRowIndex, maximumRows);

                //return new SelectResult(
                //    Convert.ToInt32(await new SalesDal().GetDetailsCount(Convert.ToInt32(iD)),
                //    await new SalesDal().SalesDetailsDataPagination(startRowIndex,maximumRows)
                //    ));

                return new SelectResult(Convert.ToInt32(count), getData);
            }
            else
            {
                return null;
            }


        }

        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_MVP_Pattern_Sales_Data.View
{
    public partial class Default : ISalesView
    {
        public int SearchSalesOrderNoBinding
        {
            get
            {
                return
                    Convert.ToInt32(txtSearchSalesOrderNo.Text);
            }
            set
            {
                txtSearchSalesOrderNo.Text = Convert.ToString(value);
            }
        }

        public string  SalesOrderNumberBinding
        {
            get
            {
                return
                    txtSalesOrderNumber.Text;
            }
            set
            {
                txtSalesOrderNumber.Text = value;
            }
        }

        public string OrderDateBinding
        {
            get
            {
                return
                    txtOrderDate.Text;
            }
            set
            {
                txtOrderDate.Text = value;
            }
        }

        public string AccountNoBinding
        {
            get
            {
                return
                    txtAccountNo.Text;
            }
            set
            {
                txtAccountNo.Text = value;
            }
        }

        public string CustomerAccountNoBinding
        {
            get
            {
                return
                    txtCustomerAccountNo.Text;
            }
            set
            {
                txtCustomerAccountNo.Text = value;
            }
        }

        public string TerritoryNameBinding
        {
            get
            {
                return
                    txtTerritoryName.Text;
            }
            set
            {
                txtTerritoryName.Text = value;
            }
        }

        public string CountryBinding
        {
            get
            {
                return
                    txtCountry.Text;
            }
            set
            {
                txtCountry.Text = value;
            }
        }

        public string CountryGroupBinding
        {
            get
            {
                return
                    txtCountryGroup.Text;
            }
            set
            {
                txtCountryGroup.Text = value;
            }
        }

        public decimal? SubTotalBinding
        {
            get
            {
                return
                     Convert.ToDecimal(txtSubTotal.Text);
            }
            set
            {
                txtSubTotal.Text = Convert.ToString(value);
            }
        }

        public decimal? TaxAmtBinding
        {
            get
            {
                return
                     Convert.ToDecimal(txtTaxAmt.Text);
            }
            set
            {
                txtTaxAmt.Text = Convert.ToString(value);
            }
        }

        public decimal? FreightBinding
        {
            get
            {
                return
                     Convert.ToDecimal(txtFreight.Text);
            }
            set
            {
                txtFreight.Text = Convert.ToString(value);
            }
        }



    }
}